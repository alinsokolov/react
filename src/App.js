import { useState } from "react"
import ToDo from "./ToDo"
import TodoForm from "./ToDoForm"

function App() {
  const [todos, setTodos] = useState([])

  const addTask = (userInput) => {
    if(userInput){
      const newItem = {
        id: Math.random().toString(36).substr(2,9),
        task: userInput,
        complete: false
      }
      setTodos([...todos, newItem])
    }
  }

  const removeTask = (id) => {
    setTodos([...todos.filter((todo) => todo.id !== id)])
  }

  const handleToglle = (id) => {
    setTodos([
      ...todos.map((todo) =>
        todo.id === id ? {...todo, complete: !todo.complete} : {...todo }

      )
    ])
  }

  const editTask = (id,value) => {
    const index = todos.findIndex((x) => x.id === id)
    const clone = [...todos]
    clone[index].task = value
    setTodos(clone)
  }

  const saveTask = () => {
    console.log(todos)
  }


  return (
    <div className="App">
      <header>
        <h1>React Todo-list</h1>
      </header>
      <TodoForm addTask={addTask}></TodoForm>
      <div className="count">{todos.length} tasks:</div>
      {
        todos.map((todo) => {
          return (
            <ToDo 
              todo={todo} 
              key={todo.id} 
              toggleTask={handleToglle}
              removeTask={removeTask}
              onEdit = {editTask}
              onSave = {saveTask}
              >
            </ToDo>
          )
        })
      }
    </div>
  );
}

export default App;
