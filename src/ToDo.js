import { useDebugValue } from "react"

function ToDo({todo, toggleTask, removeTask, onEdit,onSave}){
    const handleChange = (e) => {
        onEdit(todo.id,e.currentTarget.value)
    }
    return (
        <div key={todo.id} className="item-todo">
            <input 
                className={todo.complete ? "item-text strike" : "item-text"}
                value = {todo.task}
                onChange = {handleChange}
            >
            </input>
            <div className="item-delete" onClick={() => removeTask(todo.id)}>
                <button className="input-btn btn-red">X</button>
            </div>
            <div className="item-done" onClick={() => toggleTask(todo.id)}>
                <button className="input-btn btn-green">V</button>
            </div>
            <div className="item-save" onClick={() => onSave()}>
                <button className="input-btn btn-yellow">S</button>
            </div>
        </div>
    )
}

export default ToDo